# rpi2hosts

A small program that reads domainlists from a file and converts them to blocking
hostsfiles.

# Build

Type ```make```, ```make -f Makefile.clang``` or ```make -f Makefile.tcc```

# Install

```make install```

Customize the PREFIX variable in the Makefile, for different installation
locations or install manually with install -m755 rpi2hosts PREFIX

# Performance

Converts 35 MiB in roughly 5 seconds (if printing to stdout).
