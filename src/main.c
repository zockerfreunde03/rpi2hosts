/*
 *  Copyright (C) 2022  zockerfreunde03/z0gg3r
 *  SPDX-FileCopyrightText: 2022 zocker <zocker@10zen.eu>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *
 *  rpi2hosts - A domainlist to hostsfile converter
 *  Copyright (C) 2022  zocker
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License Version 3 or
 *  later as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License or the LICENSE file for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "input.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Shortcut to compare short and long options
#define EQUALS(x, y, z) !strcmp(x, y) || !strcmp(x, z)
// Shortcut to print usage information (w/o positional argument)
#define PRINT_USAGE(x) \
	fprintf(stderr, "%s [--input FILE] [--output FILE] [--help]\n", x)

typedef struct options_t {
	char *input;
	char *output;
	int _help;
} options_t;

options_t *parse(int, char **);
char *last_arg(int, char **);

/*
 * Takes arg(c/v) and iterates over them seraching for
 * defined options. Parsing is (very likely) not POSIX
 * compliant.
 *
 * Arguments are valid everywhere, except when they
 * are placed directly after an argument that takes
 * an option (like --input). If no input file was
 * specified, the last non-argument string of argv
 * is chosen as input file instead.
 *
 * Invalid arguments are treated as non-argument
 * string memebers of argv, thus invalid options
 * do not result in any errors, error messages and/or
 * similar things.
 *
 * If o->help is non-zero, the help argument was used
 * and all other arguments are to be ignored. All arg-
 * uments up until the -h argument are, however, parsed
 * and thus may generate error messages.
 *
 * Returns NULL if it failed to allocate enough space for
 * the struct holding the arguments or if it encountered
 * an error.
 *
 * Returns an option_t pointer otherwise. This pointer needs
 * to be free'd.
 */

options_t *parse(int argc, char **argv)
{
	options_t *o = calloc(1, sizeof(options_t));

	if (!o) {
		perror("rpi2hosts - parse");
		free(o);

		return NULL;
	}

	char *err = "";
	int input_flag = 0;
	int output_flag = 0;
	int help_flag = 0;

	for (int i = 0; i < argc; ++i) {
		if (EQUALS(argv[i], "-i", "--input")) {
			if ((i + 1) < argc) {
				input_flag = 1;
				o->input = argv[i + 1];
				++i;
			} else {
				err = "--input requires an argument!";
				goto abort;
			}
		} else if (EQUALS(argv[i], "-o", "--output")) {
			if ((i + 1) < argc) {
				output_flag = 1;
				o->output = argv[i + 1];
				++i;
			} else {
				err = "--output requires an argument";
				goto abort;
			}
		} else if (EQUALS(argv[i], "-h", "--help")) {
			help_flag = 1;
			o->_help = help_flag;

			goto early_exit;
		}
	}

	if (!input_flag) {
		o->input = last_arg(argc, argv);

		if (!o->input) {
			err = "Please provide a file to read from";
			goto abort;
		}
	}

	if (!output_flag)
		o->output = NULL;

early_exit:
	return o;

abort:
	fprintf(stderr, "%s\n", err);

	free(o);

	return NULL;
}

/*
 * Search argv for the (positional) last string,
 * that is not an argument.
 *
 * Returns the found argument or NULL if none
 * was found.
 */
char *last_arg(int argc, char **argv)
{
	char *arg = NULL;

	for (int i = 1; i < argc; ++i) {
		if (EQUALS(argv[i], "--input", "-i") ||
		    EQUALS(argv[i], "--output", "-o"))
			goto skip;
		else if (EQUALS(argv[i], "--help", "-h"))
			continue;

		arg = argv[i];
		continue;
skip:
		++i;
	}

	return arg;
}

int main(int argc, char **argv)
{
	int ret = EXIT_SUCCESS;
	options_t *o = parse(argc, argv);

	if (!o) {
		PRINT_USAGE(argv[0]);

		ret = EXIT_FAILURE;

		goto free_none;
	}

	if (o->_help) {
		PRINT_USAGE(argv[0]);

		goto free_one;
	}

	char *input = read_file(o->input);

	if (!input) {
		ret = EXIT_FAILURE;

		goto free_both;
	}

	print_hosts(o->output, input);

free_both: // If both input and o were allocated / Also normal exit
	free(input);
free_one: // If only o was allocated
	free(o);
free_none: // If nothing was allocated
	return ret;
}
