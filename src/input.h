/*
 *  Copyright (C) 2022  zockerfreunde03/z0gg3r
 *  SPDX-FileCopyrightText: 2022 zocker <zocker@10zen.eu>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *
 *  rpi2hosts - A domainlist to hostsfile converter
 *  Copyright (C) 2022  zocker
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License Version 3 or
 *  later as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License or the LICENSE file for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef INPUT_H
#define INPUT_H

#include <sys/types.h>

#define TOKEN_DELIM "\r\n"

char *read_file(const char *);
void print_hosts(char *, char *);

// Helper functions
off_t file_size(const char *);

#endif
