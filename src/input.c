/*
 *  Copyright (C) 2022  zockerfreunde03/z0gg3r
 *  SPDX-FileCopyrightText: 2022 zocker <zocker@10zen.eu>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *
 *  rpi2hosts - A domainlist to hostsfile converter
 *  Copyright (C) 2022  zocker
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License Version 3 or
 *  later as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License or the LICENSE file for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "input.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

/*
 * Uses sys/stat.h to get the size of the file at path
 * Returns -1 on error
 */
off_t file_size(const char *path)
{
	struct stat st;

	if (stat(path, &st) == 0)
		return st.st_size;

	return -1;
}

/*
 * Read the contents of `path`
 * Returns either the contents of `path` or NULL on any error
 */
char *read_file(const char *path)
{
	off_t _file_size = file_size(path);

	if (_file_size == -1) {
		perror("rpi2host - read_file");

		return NULL;
	}

	FILE *fp = fopen(path, "r");

	if (!fp) {
		perror("rpi2host - read_file");

		return NULL;
	}

	char *input = calloc(_file_size + 1, sizeof(char));

	if (!input)
		goto error;

	int num_read = fread(input, sizeof(char), _file_size, fp);

	if (num_read < _file_size)
		goto error;

	if (ferror(fp))
		goto error;

	fclose(fp);

	return input;

error:
	perror("rpi2host - read_file");

	fclose(fp);
	free(input);

	return NULL;
}

/*
 * Take the input that read_file returned and print it prefixed
 * with 0.0.0.0; If path is not NULL, open it with w+ and write
 * to it instead of stdout.
 */
void print_hosts(char *path, char *input)
{
	FILE *fp = NULL;
	if (path) {
		fp = fopen(path, "w+");

		if (!fp) {
			perror("rpi2hosts - print_hosts");

			fp = stdout;
		}
	} else {
		fp = stdout;
	}

	char *entry = strtok(input, TOKEN_DELIM);

	while (entry != NULL) {
		fprintf(fp, "0.0.0.0\t\t%s\n", entry);
		entry = strtok(NULL, TOKEN_DELIM);
	}

	if (path && fp != stdout)
		fclose(fp);
}
