CC=tcc
CFLAGS=-Wall -Wextra -std=c99 \
       -pipe -march=native -O2 \
       -fstack-protector-strong \
       -fcf-protection
LIBS=
PREFIX=/usr/local/bin

OBJQ = input.o main.o

%.o: src/%.c
	@echo CC $^
	@$(CC) -c -o $@ $< $(CFLAGS)

rpi2hosts: $(OBJQ)
	@echo CC $^
	@$(CC) -o $@ $^ $(CLFAGS) $(LIBS)
	@rm -f *.o

clean:
	@echo Removing binaries
	@rm -f rpi2hosts *.o

install:
	@echo INSTALL rpi2hosts
	@install -m755 rpi2hosts $(PREFIX)/rpi2hosts
